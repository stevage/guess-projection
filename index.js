const epsg = require('epsg');
const proj4lib = require('proj4');
const proj4 = proj4lib.default || proj4lib;
const turf = require('@turf/turf');

const blacklist = ['EPSG:900914'];
module.exports = function(sourceX, sourceY, targetLng, targetLat) {
    const results = [];
    Object.keys(epsg)
    .filter(code => code.match(/^EPSG/) && blacklist.indexOf(code) === -1)
    .forEach(projection => {
        try {
            const def = epsg[projection];
            const result = proj4(def, epsg['EPSG:4326'], [sourceX, sourceY]);
            const distance = turf.distance(turf.point(result), turf.point([targetLng, targetLat]), { units: 'kilometers' });
            results.push({ projection, distance, def });
        } catch (e) { 

        }
    });
    return results.sort((a, b) => a.distance - b.distance);
}
