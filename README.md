### Guess projection

Find the geographic projection which best turns a given (X,Y) into an expected (lng, lat).

Returns an array of { projection, distance, def }, reverse sorted by distance. So the best guess for the name of the projection is `results[0].projection`.

```
const guess = require('guess-projection');
const results = guess(16137423.630081, -4553353.774948, 144.9, -37.85)
console.log(results[0]);
// {
//    "projection": "EPSG:900914",
//    "distance": 6.891712664568894,
//    "def": "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"
//  }
```

You can see a working example at https://stevage.github.io/WhatTheProj/