const guess = require('./index');
describe('Guesses Web Mercator', () => {
    const results = guess(16137423, -4553353, 144.9, -37.85);
    test('Gets results', () => expect(results.length).toBeGreaterThan(1));
    test('Gets projection', () => expect(results[0].projection).toMatch(/EPSG:900913/));
    test('Gets distance', () => expect(results[0].distance).toBeCloseTo(6.89));
});

describe('Guesses GDA94', () => {
    const results = guess(295114.372422643, 5807879.97589356, 144.650115, -37.897072);
    test('Gets results', () => expect(results.length).toBeGreaterThan(1));
    test('Gets projection', () => expect(results[0].projection).toMatch('EPSG:28355'));
    test('Gets distance', () => expect(results[0].distance).toBeCloseTo(5.18));
});

test('Ranks results in order', () => {
    const results = guess(16137423, -4553353, 144.9, -37.85);
    let notSorted = false;
    results.slice(1).forEach((r, i) => {
        if (results[i].distance > r.distance) {
            notSorted = true;
        }
    });
    expect(notSorted).toBeFalsy();
});

test('Returns an array no matter what.', () => {
    const results = guess(NaN, NaN);
    expect(results.length).toBe(0);
});
